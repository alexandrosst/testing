from PCA9685 import PCA9685
import threading
import time
import zmq

# movement 0: 'Forward' - 'Backward' for closing & opening grabber
# movement 1: 'TurnLeft' - 'TurnRight' for rotating left & right robotic arm
# movement 2: 'Up' - 'Down' for moving robotic arm up & down
# movement 3: 'Right' - 'Left' for moving robotic arm back & forth

class RoboticArm:
    def __init__(self, address=0x40, freq=50, servo0_angle=30, servo23_angle=10):
        # Initialize the PCA9685 module
        self.pwm = PCA9685(address)

        # Set the frequency for the PWM signal
        self.pwm.setPWMFreq(freq)

        # Calculate the pulses based on given angles in degrees
        servo0_pulse = 500/45*servo0_angle + 500
        servo23_pulse = 500/45*servo23_angle + 500
        
        # Define the limits for each servo
        self.limits = [[500, 2500 - servo0_pulse], [500, 2500], [500 + servo23_pulse, 2500 - servo23_pulse], [500 + servo23_pulse, 2500 - servo23_pulse]]
        
        # Initialize the positions for each servo
        self.positions = [500, 500, 1000, 1500]

        # Initialize the steps for each servo
        self.steps = [0, 0, 0, 0]

        # Define the movements and their corresponding servo index and step
        self.movements = {
            "Forward": (0, -35),
            "Backward": (0, 35),
            "TurnLeft": (1, -20),
            "TurnRight": (1, 20),
            "Up": (2, 15),
            "Down": (2, -15),
            "Left": (3, 5),
            "Right": (3, -5),
            "Stop": (None, 0),
            "Start": (None, None)
        }

        # Initialize the last movement
        self.last_movement = None

        # Initialize the ZeroMQ context
        self.context = zmq.Context()

        # Create a REP socket
        self.socket = self.context.socket(zmq.REP)

        # Bind the socket to a specific address
        self.socket.bind("tcp://*:5580")

        # Initialize the running state
        self.running = True

        # Set the initial position for each servo
        for i, pos in enumerate(self.positions):
            self.pwm.setServoPulse(i, pos)

    def set_movement(self, movement):
        # Set the movement for the robotic arm
        # This function changes the steps for the specified movement
        if movement in self.movements:
            # Get the index and step for the movement
            index, step = self.movements[movement]
            # If the index is None, it means the movement is either 'Stop' or 'Start'
            if index is None:
                # If the step is not None, it means the movement is 'Stop'
                if step is not None:
                    # Set all steps to 0 and stop the movement
                    self.steps = [step, step, step, step]
                    self.running = False
                # If the step is None, it means the movement is 'Start'
                else:
                    # get the robotic arm rolling by restoring the last movement
                    self.running = True
                    self.set_movement(self.last_movement)
            # If the robotic arm is running, set the step for the specified index
            elif self.running:
                self.steps[index] = step

    def move(self):
        # Move the robotic arm
        # This function automatically updates the position based on the steps
        for i, step in enumerate(self.steps):
            # If the step is not 0, it means there is a movement
            if step != 0:
                # Update the position based on the step
                self.positions[i] += step
                # If the position is greater than the limit, set it to the limit
                if self.positions[i] >= self.limits[i][1]: 
                    self.positions[i] = self.limits[i][1]
                # If the position is less than the limit, set it to the limit
                if self.positions[i] <= self.limits[i][0]:
                    self.positions[i] = self.limits[i][0]
                # Set the servo pulse based on the position
                self.pwm.setServoPulse(i, self.positions[i])

        # Start a timer to call the move function again after 0.02 seconds
        self.t = threading.Timer(0.02, self.move)
        self.t.start()
    
    def listen(self):
        while True:
            # Wait for next request from client
            message = self.socket.recv_string()
            print(f"Received request: {message}")
            
            # Set movement based on received message
            self.set_movement(message)

            # Send reply back to client
            # message is either 'Start' or 'Stop'
            if message == "Start":
                self.socket.send_string("Robotic Arm has started moving!")
            else:
                self.socket.send_string("Robotic Arm has halted!")

if __name__ == "__main__" :
    # Create an instance of the RoboticArm class
    arm = RoboticArm()
    # Create a timer that calls the move function after 0.02 seconds
    arm.t = threading.Timer(0.02, arm.move)
    # Set the timer as a daemon so it stops when the main program stops
    arm.t.daemon = True
    # Start the timer
    arm.t.start()

    # Create a new thread that listens for incoming requests
    listen_thread = threading.Thread(target=arm.listen)
    # Set the thread as a daemon so it stops when the main program stops
    listen_thread.daemon = True
    # Start the thread
    listen_thread.start()

    while True:
        # Normal pi routing: sequence of movements to form a 'pi' shape
        for m in ["Backward", "Up", "TurnRight", "Down", "Forward"]:
            # Wait in case the robotic arm is halted
            while not arm.running :
                time.sleep(1)
            # Set the last_ovement to the current one and start the movement
            arm.last_movement = m
            arm.set_movement(m)
            time.sleep(2.5)

        # Reverse pi routing: sequence of movements to form a 'pi' shape in reverse
        for m in ["Backward", "Up", "TurnLeft", "Down", "Forward"]:
            # Wait in case the robotic arm is halted
            while not arm.running :
                time.sleep(1)
            # Set the last_ovement to the current one and start the movement
            arm.last_movement = m
            arm.set_movement(m)
            time.sleep(2.5)