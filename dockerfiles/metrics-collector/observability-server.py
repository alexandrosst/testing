from flask import Flask, request
import json
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from prometheus_client import make_wsgi_app, Counter, Histogram


app = Flask(__name__)
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {
    '/metrics': make_wsgi_app()
})
REQUEST_COUNT = Counter(
    'app_request_count',
    'Application Request Count',
    ['method', 'endpoint', 'http_status']
)
LATENCY = Histogram(
    'latency_nanoseconds',
    'Latency',
    ['type', 'service_or_link']
)
@app.route("/")
def server():
    REQUEST_COUNT.labels('GET', '/', 200).inc()
    timestamps = json.loads(request.headers.get('timestamps'))
    print("Timestamps:", timestamps)

    # (split) sender ---> (frame) sampler ---> (object) detector
    LATENCY.labels('communication', 'sender-sampler').observe(timestamps[1] - timestamps[0])
    LATENCY.labels('computation', 'sampler').observe(timestamps[2] - timestamps[1])
    LATENCY.labels('communication', 'sampler-detector').observe(timestamps[3] - timestamps[2])
    LATENCY.labels('computation', 'detector').observe(timestamps[4] - timestamps[3])

    return "Success",200


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=5502, threaded=True)