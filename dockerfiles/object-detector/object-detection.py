import numpy as np
from ultralytics import YOLO
import zmq
from PIL import Image
import io
import time
import requests
import json
import struct
import os
import torch

def communicate_with_robotic_arm(socket, command):
    # Send 'Stop' or 'Start' command to the robotic arm
    socket.send_string(command)

    # Wait for a reply from the robotic arm for up to 5 seconds
    if socket.poll(timeout=5000):
        message = socket.recv_string()
        print(f"Received reply: {message}")
    else:
        print("No reply received. The message may not have been sent.")

def detect(port, model_name, num_threads, destination, robotic_arm_ip, robotic_arm_port):
    # load a pretrained YOLOv8 model: yolov8n.pt, yolov8s.pt, yolov8m.pt, yolov8l.pt, or yolov8x.pt
    # yolov8n.pt is the smallest and fastest model, while yolov8x.pt is the largest and most accurate
    torch.set_num_threads(num_threads)
    model = YOLO(model_name)

    context = zmq.Context()
    # create a REP socket
    socket = context.socket(zmq.REP)
    # bind to the appropriate IP and port
    socket.bind(f"tcp://*:{port}")

    # Create a REQ socket for communicating with the robotic arm
    robotic_arm_socket = context.socket(zmq.REQ)
    # Connect to the robotic arm using the IP address and port number
    robotic_arm_socket.connect(f"tcp://{robotic_arm_ip}:{robotic_arm_port}")
    is_arm_halted=False

    try:
        while True:
            # receive the JPEG frame
            jpeg_data = socket.recv()

            # get current time, i.e., fourth timestamp
            ts_4 = time.time_ns()

            ts_3_bytes = jpeg_data[-8:]
            # unpack the id from bytes to a number
            ts_3 = struct.unpack('!Q', ts_3_bytes)[0]

            ts_2_bytes = jpeg_data[-16:-8]
            # unpack the id from bytes to a number
            ts_2 = struct.unpack('!Q', ts_2_bytes)[0]
            
            ts_1_bytes = jpeg_data[-24:-16]
            # unpack the id from bytes to a number
            ts_1 = struct.unpack('!Q', ts_1_bytes)[0]
            
            id_bytes = jpeg_data[-32:-24]
            # unpack the id from bytes to a number
            frame_id = struct.unpack('!Q', id_bytes)[0]

            if ts_1 == 0:
                print("Ignoring repeated frame!\n")
                socket.send_string("Ignored")
                continue

            print(f"Frame ID: {frame_id}\nTimestamp 1: {ts_1}\nTimestamp 2: {ts_2}\nTimestamp 3: {ts_3}\nTimestamp 4: {ts_4}")

            jpeg_data = jpeg_data[:-32]

            # send reply back to client
            socket.send_string("OK")

            # # decode JPEG data using OpenCV
            # img = cv2.imdecode(np.frombuffer(jpeg_data, dtype=np.uint8), cv2.IMREAD_COLOR)
            # # e.g., img.shape: (720, 1280, 3)
            # height, width = img.shape[:2]

            # # img dimensions must be a multiple of max stride 32
            # new_height = (height // 32) * 32
            # new_width = (width // 32) * 32
            # resized_img = cv2.resize(img, (new_width, new_height))

            # decode JPEG data using PIL
            img = Image.open(io.BytesIO(jpeg_data))

            # resize the image so its dimensions are multiples of 32
            width, height = img.size
            new_width = (width // 32) * 32
            new_height = (height // 32) * 32
            img = img.resize((new_width, new_height))

            # convert the PIL Image to a numpy array
            img = np.array(img)

            # use YOLO to do the inference
            # result = model.predict(img)[0] # we provided only one image
            result = model.predict(img, imgsz=(new_height, new_width))[0] # set specific input image size

            # https://www.freecodecamp.org/news/how-to-detect-objects-in-images-using-yolov8/
            detected_objects = []
            for box in result.boxes:
                # the class of the detected object
                obj_class = result.names[box.cls[0].item()]
                # the confidence level of the model about this object
                # conf = round(box.conf[0].item(), 2)
                # detected_objects.append((obj_class, conf))
                detected_objects.append(obj_class)
            # print(detected_objects)

            # Example: an alert should be generated if a person is detected in the location
            if 'person' in detected_objects:
                # TODO: stop the operation of the robotic arm
                print('ALERT!!!')
                if not is_arm_halted:
                    communicate_with_robotic_arm(robotic_arm_socket, "Stop")
                    is_arm_halted = True
            else:
                if is_arm_halted:
                    communicate_with_robotic_arm(robotic_arm_socket, "Start")
                    is_arm_halted = False

            ts_5 = time.time_ns()
            # make an HTTP request to the Flask server
            headers = {'timestamps': json.dumps([ts_1, ts_2, ts_3, ts_4, ts_5])}
            response = requests.get(f'http://{destination}:5502', headers=headers)

    except KeyboardInterrupt:
        print("Interrupt received, stopping...")
    finally:
        # clean up here
        socket.close()
        context.term()

if __name__ == '__main__':
    args = {'port': os.getenv('port'), 
        'yolo_model': os.getenv('yolo_model'),
        'num_threads': int(os.getenv('num_threads')),
        'destination': os.getenv('destination'),
        'robotic_arm_ip': os.getenv('robotic_arm_ip'),
        'robotic_arm_port': os.getenv('robotic_arm_port')
    }

    print(f'Receiving sampled frames at UDP port {args["port"]}')
    print(f'Using pre-trained model: {args["yolo_model"]}')
    print(f'Number of threads: {args["num_threads"]}')
    print(f'Robotic arm IP: {args["robotic_arm_ip"]}')
    print(f'Robotic arm port: {args["robotic_arm_port"]}')
    
    detect(args["port"], args["yolo_model"], args["num_threads"], args["destination"], args["robotic_arm_ip"], args["robotic_arm_port"])